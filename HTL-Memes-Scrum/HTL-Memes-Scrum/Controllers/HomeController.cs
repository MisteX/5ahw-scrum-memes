﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Testprogramm.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Start()
        {
            return View();
        }

        public ActionResult Latest()
        {
            return View();
        }

        public ActionResult Favorite()
        {
            return View();
        }

        public ActionResult Upload()
        {
            return View();
        }

        public ActionResult SignIn()
        {
            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }

        public ActionResult Imprint()
        {
            return View();
        }

        public ActionResult DataProtection()
        {
            return View();
        }
    }
}