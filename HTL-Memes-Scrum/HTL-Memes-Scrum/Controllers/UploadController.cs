﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HTL_Memes_Scrum.Models;

namespace test.Controllers {
    public class UploadController : Controller {
        // GET: Upload
        
        public ActionResult Upload() {
            return View();
        }

        [HttpPost]
        public ActionResult Upload(HttpPostedFileBase file) {

            var path = "";
            if (file != null) {
                if (file.ContentLength > 0) {
                    //Überprüfen ob es ein bildformat ist
                    if ((Path.GetExtension(file.FileName).ToLower() == ".jpg") || (Path.GetExtension(file.FileName).ToLower() == ".jpeg")) {
                        path = Path.Combine(Server.MapPath("~/Memes"), file.FileName);
                        file.SaveAs(path);
                        //ViewBag.UploadSuccess == true;
                    }
                }
            }
            return View();
        }
    }
}