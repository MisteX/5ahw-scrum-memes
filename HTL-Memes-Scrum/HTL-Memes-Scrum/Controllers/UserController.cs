﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using HTL_Memes_Scrum.Models;
using HTL_Memes_Scrum.Models.models;

namespace HTL_Memes_Scrum.Controllers
{
    public class UserController : Controller
    {
        [HttpGet]
        public ActionResult Login()
        {
            LoginUser lu = new LoginUser();
            return View(lu);
        }
        [HttpPost]
        public ActionResult Login(LoginUser lu)
        {

            return View();
        }

        [HttpGet]
        public ActionResult Register()
        {
            return View();
        }


        public ActionResult UserEditor()
        {
            if ((Session["Admin"] != null) && (Convert.ToInt32(Session["Admin"]) == 1)) ;
            try
            {
                UserRepository dbm = UserRepository.GetInstance();

                return View(dbm.GetAllUsers());

            }
            catch (SqlException)
            {
                return View("Message", new Message("Datenbankfehler!", "", "Die Datenbank reagierte nicht!", "Bitte später erneut versuchen!"));
            }

        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            try
            {
                UserRepository dbm = UserRepository.GetInstance();

                if (dbm.DeleteUser(id))
                {
                    return RedirectToAction("UserEditor");
                }
                else
                {
                    return View("Message", new Message("Benutzer löschen", "", "Der Benutzer konnte nicht gelöscht werden.", ""));
                }
            }
            catch (SqlException)
            {
                return View("Message", new Message("Datenbankfehler", "", "Probleme mit der Datenbank.", "Versuchen Sie es später erneut."));
            }
        }

        [HttpPost]
        public ActionResult Update(int id)
        {
            try
            {
                UserRepository dbm = UserRepository.GetInstance();

                if(dbm.GetUser(id) != null)
                {
                    return View(dbm.GetUser(id));
                }
                else
                {
                    return View("Message", new Message("Benutzer nicht vorhanden!", "", "Der Benutzer konnte nicht gefudnen werden.", ""));
                }
            }
            catch (MySqlException)
            {
                return View("Message", new Message("Datenbankfehler", "", "Probleme mit der Datenbank.", "Versuchen Sie es später erneut."));
            }
        }

        [HttpPost]
        public ActionResult Update(User user)
        {
            try
            {
                UserRepository dbm = UserRepository.GetInstance();

                if (dbm.GetUser(user.ID) != null)
                {
                    dbm.UpdateUser(user.ID, user);
                }
                else
                {
                    return View("Message", new Message("Benutzer nicht vollständig definiert!", "", "Bitte alle geforderten Daten angeben.", ""));
                }
            }
            catch (MySqlException)
            {
                return View("Message", new Message("Datenbankfehler", "", "Probleme mit der Datenbank.", "Versuchen Sie es später erneut."));
            }
        }
    }

}