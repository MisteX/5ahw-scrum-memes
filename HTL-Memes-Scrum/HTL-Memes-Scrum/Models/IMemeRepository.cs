﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HTL_Memes_Scrum.Models;

namespace TestApplication.Models
{
    interface IMemeRepository
    {
        void Open();
        void Close();

        bool AddVote(Vote voteToAdd);

        int getUpvotesForMeme(int memeId);

        int getDownvotesForMeme(int memeId);

        bool AddTag(Tag tagToAdd);
    }
}
