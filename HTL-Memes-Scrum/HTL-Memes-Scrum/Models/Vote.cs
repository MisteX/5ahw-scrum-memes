﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTL_Memes_Scrum.Models {
    public class Vote {
        private string v1;
        private string v2;

        public bool isUpvote { get; set; }
        public string userName { get; set; }

        public Vote() : this("", "") { }
        public Vote(bool isUpvote, string userName) {
            this.isUpvote = isUpvote;
            this.userName = userName;
        }

        public Vote(string v1, string v2)
        {
            this.v1 = v1;
            this.v2 = v2;
        }
    }
}