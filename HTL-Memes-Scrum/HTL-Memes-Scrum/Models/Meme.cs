﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTL_Memes_Scrum.Models {
    public class Meme {
        public String memePath { get; set;}
        public String memeHeading { get; set; }
        public DateTime memeUploadDate { get; set; }
        public List<User> likeList = new List<User>();
        public List<String> tagList = new List<String>();
        public List<String> comments = new List<String>();


        public Meme() : this("", "", DateTime.Now) { }
        public Meme(String memeP, String memeH, DateTime memeD) {
            this.memePath = memeP;
            this.memeHeading = memeH;
            this.memeUploadDate = memeD;
        }

        public void addLike(User u) {
            this.likeList.Add(u);
        }

        public void addTag(string tag) {
            this.tagList.Add(tag);
        }
        
        public void addComment(string comment) {
        
        Console.WriteLine("Test");
            this.comments.Add(comment);  
            
            
            
        }
    }
}