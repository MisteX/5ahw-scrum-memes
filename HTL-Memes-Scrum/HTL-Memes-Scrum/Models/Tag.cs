﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestApplication.Models
{
    public class Tag
    {
        private int _tagId;
        private int _memeId;

        public int TagId {
            get { return this._memeId; }
            set
            {
                if (value > 0) {
                    this._tagId = value;
                }
            }

        }
        public string TagName { get; set; }

        public int MemeId {
            get { return this._memeId; }
            set
            {
                if(value > 0)
                {
                    this._memeId = value;
                }
            }
        }

        public Tag() : this(0, "", 0) { }

        public Tag(int tagId, string tagName, int memeId) {
            this.TagId = tagId;
            this.TagName = tagName;
            this.MemeId = memeId;
        }

    }
}