﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTL_Memes_Scrum.Models
{
    public class User
    {

        private int _id;

        public int ID
        {
            get { return this._id; }

        }
        public string Email { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public DateTime Birthdate { get; set; }
        public bool isAdmin { get; set; }
        List<object> Memes;

        public User() : this("", "", "", "", "", DateTime.Now, false, null) { }
        public User(string email, string username, string password, string firstname, string lastname, DateTime birthdate, bool isadmin, List<object> list)
        {
            this.Email = email;
            this.Username = username;
            this.Password = password;
            this.Firstname = password;
            this.Lastname = lastname;
            this.Birthdate = birthdate;
            this.isAdmin = isadmin;
            this.Memes = list;
        }
        
        //Memes Liste muss aus der DB geholt werden (mit Repository)

    }
}