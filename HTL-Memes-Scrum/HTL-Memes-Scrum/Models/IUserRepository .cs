﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using HTL_Memes_Scrum.Models;

namespace Repository
{
    interface IUserRepository
    {
        void Open();

        void Close();

        List<User> GetAllUsers();

        List<User> GetAllAdmins();

        List<User> GetAllCustomers();

        User FindUserPerEmail(string email);

        List<User> FindUserPerBirthYear(int birthYear);

        List<User> FindUserPerGender(char gender);

        List<User> FindUserPerFirstName(string firstName);

        List<User> FindUserPerLastName(string lastName);

        List<User> FindUserPerName(string firstname, string lastName);

        List<User> FindUserPerCity(string city);

        List<User> FindUserPerCountry(string country);

        bool InsertUser(User userToInsert);

        bool RemoveUser(string emailToRemove);

        bool ChangeUserData(string emailToChange, User newUserData);

        List<User> SortOutDuplicateEntries(List<List<User>> allFoundUsers);

        // ...
    }
}
