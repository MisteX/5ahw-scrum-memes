﻿namespace HTL_Memes_Scrum.Controllers
{
    public class LoginUser
    {

        public string Name { get; set; }
        public string Password { get; set; }

        public LoginUser() : this ("","") { }

        public LoginUser(string name, string pw)
        {
            this.Name = name;
            this.Password = pw;
        }
    }
}