﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
using TestApplication.Models;
using HTL_Memes_Scrum.Models;

namespace TestApplication.Models
{
    public class MemeRepository : IMemeRepository
    {
        private string _connStrMySQL = "Server=localhost;Database=memedb;Uid=root;Pwd=password";
        private MySqlConnection _connection;

        public void Open()
        {
            try
            {
                if (this._connection == null)
                {
                    this._connection = new MySqlConnection(this._connStrMySQL);
                }
                this._connection.Open();
                
            }
            catch (MySqlException)
            {
                throw;
            }

        }
        public void Close()
        {

            try
            {
                if (this._connection != null )
                {
                    this._connection.Close();
                    this._connection = null;
                }
            }
            catch (MySqlException)
            {
                throw;
            }

        }

        //public bool AddVote(Vote voteToAdd)
        //{
        //    try
        //    {
        //        MySqlCommand cmd = this._connection.CreateCommand();
        //        cmd.CommandText = "INSERT INTO vote(`memeId`,`userId`,`isUpvote`) VALUES(@memeId, @userId, @isUpvote)";
        //        cmd.Parameters.AddWithValue("memeId", voteToAdd.MemeId);
        //        cmd.Parameters.AddWithValue("userId", voteToAdd.UserId);
        //        cmd.Parameters.AddWithValue("isUpvote", voteToAdd.IsUpvote);

        //        if (cmd.ExecuteNonQuery() == 1)
        //        {
        //            return true;
        //        }

        //    }
        //    catch(MySqlException) { throw; }
        //    return false;
        //}

        public int getUpvotesForMeme(int memeId)
        {
            int numberOfLikes = 0;
            try
            {
                MySqlCommand cmd = this._connection.CreateCommand();
                cmd.CommandText = "SELECT count(*) from vote where memeId = @memeId AND isUpvote = true";
                cmd.Parameters.AddWithValue("memeId", memeId);

                using(MySqlDataReader reader = cmd.ExecuteReader())
                {
                    reader.Read();
                    numberOfLikes = reader.GetInt32("count(*)");
                }
            }
            catch (MySqlException) { throw; }

            return numberOfLikes;
        }

        public int getDownvotesForMeme(int memeId)
        {
            int numberOfLikes = 0;
            try
            {
                MySqlCommand cmd = this._connection.CreateCommand();
                cmd.CommandText = "SELECT count(*) from vote where memeId = @memeId AND isUpvote = false";
                cmd.Parameters.AddWithValue("memeId", memeId);

                using (MySqlDataReader reader = cmd.ExecuteReader())
                {
                    reader.Read();
                    numberOfLikes = reader.GetInt32("count(*)");
                }
            }
            catch (MySqlException) { throw; }

            return numberOfLikes;
        }

        public bool AddTag(Tag tagToAdd)
        {
            bool ret = false;
            try
            {
                MySqlCommand cmd = this._connection.CreateCommand();
                cmd.CommandText = "INSERT INTO tag(tagName, memeId) VALUES(@tagname, @memeid)";
                cmd.Parameters.AddWithValue("tagname", tagToAdd.MemeId);
                cmd.Parameters.AddWithValue("memeid", tagToAdd.TagId);
                
                if(cmd.ExecuteNonQuery() == 1)
                {
                    ret = true;
                }

            }
            catch (MySqlException) { throw; }
            return ret;
        }

        public bool RegisterUser(User u)
        {
            bool ret = false;

            try
            {
                MySqlCommand cmd = this._connection.CreateCommand();
                cmd.CommandText = "INSERT INTO user(username, password, email, firstname, lastname, birthdate, isAdmin) " +
                    "VALUES(@uname, sha1(@pwd), @email, @fname, @lname, @bd, @isAd)";
                cmd.Parameters.AddWithValue("uname", u.Username);
                cmd.Parameters.AddWithValue("pwd", u.Password);
                cmd.Parameters.AddWithValue("email", u.Email);
                cmd.Parameters.AddWithValue("fname", u.Firstname);
                cmd.Parameters.AddWithValue("lname", u.Lastname);
                cmd.Parameters.AddWithValue("bd", u.Birthdate);
                cmd.Parameters.AddWithValue("isAd", u.isAdmin);

                if(cmd.ExecuteNonQuery() == 1)
                {
                    ret = true;
                }

            }catch(MySqlException ex) { throw ex; }

            return ret;
        }
   
        //public bool AddMeme(Meme memeToAdd)
        //{
        //    bool ret = false;
           
        //    try
        //    {
        //        MySqlCommand cmd = this._connection.CreateCommand();
        //        cmd.CommandText = "INSERT INTO meme(userId, filepath, date, title) " +
        //            "VALUES(@userId, @filepath, @date, @titles)";

        //        cmd.Parameters.AddWithValue("userId",memeToAdd.uploadedByUser);
        //        cmd.Parameters.AddWithValue("filepath", memeToAdd.memePath);
        //        cmd.Parameters.AddWithValue("date", memeToAdd.memeUploadDate);
        //        cmd.Parameters.AddWithValue("titles", memeToAdd.memeHeading);

        //        if(cmd.ExecuteNonQuery() == 1)
        //        {
        //            ret = true;
        //        }

        //    }
        //    catch (MySqlException) { throw; }

        //    return ret;
        //}

        public bool AddVote(Vote voteToAdd)
        {
            throw new NotImplementedException();
        }
    }
}