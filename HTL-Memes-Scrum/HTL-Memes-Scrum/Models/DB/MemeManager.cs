﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using HTL_Memes_Scrum.Models.DB;

using MySql.Data.MySqlClient;


namespace HTL_Memes_Scrum.Models.DB {
    public class MemeManager {
        private string _connectionString = "Server=localhost;Database=;Uid=root;Pwd=";
        private MySqlConnection _connection;
        private static MemeManager instance;

        public MemeManager getInstance() {
            if(instance == null) {
                instance = new MemeManager();
            }
            return instance;
        }

        public void Open() {
            try {
                if (this._connection == null) {
                    this._connection = new MySqlConnection(this._connectionString);
                }
                if (this._connection.State != ConnectionState.Open) {
                    this._connection.Open();
                }
            }
            catch (MySqlException) {
                throw;
            }

        }
        public void Close() {

            try {
                if (this._connection != null && this._connection.State == ConnectionState.Open) {
                    this._connection.Close();
                    this._connection = null;
                }
            }
            catch (MySqlException) {
                throw;
            }

        }

        public void deleteMemeFromDB(int userID) {

        }

        public List<Meme> getAllMemesFromUser(int userID) {
                List<Meme> mList = new List<Meme>();
            try {
                MySqlCommand cmdAuthenticate = this._connection.CreateCommand();
                cmdAuthenticate.CommandText = "SELECT * FROM meme WHERE userId = @userID";
                cmdAuthenticate.Parameters.AddWithValue("userID", userID);

                using (MySqlDataReader reader = cmdAuthenticate.ExecuteReader()) {
                    while (reader.HasRows) {
                        reader.Read();
                        //mList.Add(new Meme())
                    }

                }
            }
            catch (Exception) {
                throw;
            }
            return mList;
        }

        public List<String> getAllTagsFromMeme(int memeID) {
            List<String> tList = new List<String>();
            try {
                MySqlCommand cmdAuthenticate = this._connection.CreateCommand();
                cmdAuthenticate.CommandText = "SELECT * FROM meme WHERE memeId = @memeID";
                cmdAuthenticate.Parameters.AddWithValue("memeID", memeID);

                using (MySqlDataReader reader = cmdAuthenticate.ExecuteReader()) {
                    while (reader.HasRows) {
                        reader.Read();
                        if (reader.HasRows) {
                            reader.Read();
                            tList.Add(reader.GetString("tagName"));
                        }
                    }
                }
            }
            catch (Exception) {
                throw;
            }

            return tList;
        }

        public List<Vote> getAllVotesFromMeme(int memeID) {
            List<Vote> vList = new List<Vote>();
            try {
                MySqlCommand cmdAuthenticate = this._connection.CreateCommand();
                cmdAuthenticate.CommandText = "SELECT * FROM vote WHERE memeId = @memeID";
                cmdAuthenticate.Parameters.AddWithValue("memeID", memeID);

                using (MySqlDataReader reader = cmdAuthenticate.ExecuteReader()) {
                    while (reader.HasRows) {
                        reader.Read();
                        if (reader.HasRows) {
                            reader.Read();
                            string uName = getUsernameByID(reader.GetInt32("userId"));
                            vList.Add(new Vote(reader.GetBoolean("isUpvote"), uName));
                        }
                    }
                }
            }
            catch (Exception) {
                throw;
            }

            return vList;
        }

        public string getUsernameByID(int userID) {
            string usrName = null;
            try {
                MySqlCommand cmdAuthenticate = this._connection.CreateCommand();
                cmdAuthenticate.CommandText = "SELECT username FROM user WHERE userId = @userID";
                cmdAuthenticate.Parameters.AddWithValue("userID", userID);

                using (MySqlDataReader reader = cmdAuthenticate.ExecuteReader()) {
                    reader.Read();
                    usrName = reader.GetString("username");
                }
                }
            catch (Exception) {
                throw;
            }
            return usrName;
        }
    }
}