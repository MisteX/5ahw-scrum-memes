﻿using System.Web;
using System.Web.Mvc;

namespace HTL_Memes_Scrum
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
